local module={}

value = gpio.HIGH
local module_mode = "PROD"
local duration = 100000

function get_text (str, init, term)
   local _, start = string.find(str, init)
   local stop = string.find(str, term)
   local result = nil
   if _ and stop then
      result = string.sub(str, start + 1, stop - 1)
   end
   return result
end

function json_decoder(key_values)
  return sjson.decode("{" ..key_values .. "}")
end

function node_reconnect(s, p)

  conf = {}
  conf.ssid = s
  conf.pwd = p
  conf.save=true

  print(">>>>> node_reconnect")
  wifi.setmode(wifi.STATION)
  if(wifi.sta.clearconfig())then
    tmr.delay(1000)
  end
  wifi.sta.config(conf)
  wifi.sta.connect()
end

local function httpServer()
  config = require("config")

  print("Setting local Wifi...")
  wifi.setmode(wifi.SOFTAP)
  wifi.ap.config(config.LOCAL_WIFI)
  wifi.ap.setip(config.CONFIG_IP)
  print("SERVER: " .. wifi.ap.getip())


  print("Starting HTTP Server...")

  -- Check for opened Servers
  if srv~=nil then
    srv:close()
  end

  -- New HTTP Server
  srv = net.createServer(net.TCP)
  srv:listen(80, function (conn)
    conn:on("receive", function(conn, data)
      config = json_decoder(get_text(data, "{", "}"))
      if (config.ssid and config.password) then
        print("Got a new SSID! Let me try!")
        conn:send("<h1> WiFi FOUND! (" .. config.ssid .. ") Trying to connect... </h1>")
        node_reconnect(config.ssid, config.password)
      else
        print("NOT FOUND!")
        conn:send("<h1> Oooops! WiFi NOT FOUND!</h1>")
      end

    end)
  end)

  print("HTTP Server ready!")


end

local function register_callbacks()

  -- Define WiFi station event callbacks
  wifi_connect_event = function(T)
    print("Connection to AP("..T.SSID..") established!")
    print("Waiting for IP address...")
    if disconnect_ct ~= nil then disconnect_ct = nil end
  end

  wifi_got_ip_event = function(T)
    -- Note: Having an IP address does not mean there is internet access!
    -- Internet connectivity can be determined with net.dns.resolve().
    print("Wifi is ready! IP address is: "..T.IP)
    app.start()
  end

  wifi_disconnect_event = function(T)
    if T.reason == wifi.eventmon.reason.ASSOC_LEAVE then
      print("the station has disassociated from a previously connected AP")
      return
    end
    -- total_tries: how many times the station will attempt to connect to the AP. Should consider AP reboot duration.
    local total_tries = 120
    print("\nWiFi connection to AP("..T.SSID..") has failed!")

    --There are many possible disconnect reasons, the following iterates through
    --the list and returns the string corresponding to the disconnect reason.
    for key,val in pairs(wifi.eventmon.reason) do
      if val == T.reason then
        print("Disconnect reason: "..val.."("..key..")")
        break
      end
    end

    if disconnect_ct == nil then
      disconnect_ct = 1
    else
      disconnect_ct = disconnect_ct + 1
    end
    if disconnect_ct < total_tries then
      print("Retrying connection...(attempt "..(disconnect_ct+1).." of "..total_tries..")")
    else
      disconnect_ct = 0
      print("Aborting connection to AP! Switching to CONFIG mode. ")
      httpServer()
    end
  end

  -- Register WiFi Station event callbacks
  wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, wifi_connect_event)
  wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)
  wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)

end

local function isEmpty(s)
  return s == nil or s == ''
end

function module.startup()
  print("STARTING")
  register_callbacks()
  print("CONFIG: " .. wifi.sta.getdefaultconfig())
  if isEmpty(wifi.sta.getdefaultconfig()) then
    httpServer()
  else
      wifi.setmode(wifi.STATION)
      wifi.sta.connect()
  end
end

return module
