local module = {}
m = nil

function module.mqtt_logger(message)
    print(message)
    tm = rtctime.epoch2cal(rtctime.get())
    m:publish(config.ID .. "/log",  string.format("%04d/%02d/%02d %02d:%02d:%02d", tm["year"], tm["mon"], tm["day"], tm["hour"], tm["min"], tm["sec"]) .. " - " .. message, 0, 0)
end

function log_idle()
    mqtt_logger("Idle...")
end

function mqtt_subscribe()
    m:subscribe({
                [config.ENDPOINT .. config.ID .. "CV1"] = 0,
                [config.ENDPOINT .. config.ID .. "LT1"] = 0,
                [config.ID .. "/RST"] = 0,
                },
                function(conn)
                    mqtt_logger("Subscribed to topics!")
                end,
                function(conn, reason)
                    mqtt_logger("Error! Unable to subscribe!")
                    mqtt_logger("Reason: " .. reason)
                end)
end

function module.start()
    print("Connecting at MQTT Broker: " .. config.HOST..":"..config.PORT)
    m = mqtt.Client(config.ID, 10, config.USER, config.PASSWD)
    m:connect(config.HOST, config.PORT, 0,
        function(client)
            sntp.sync("pool.ntp.org",
                function()
                    time_now = rtctime.epoch2cal(rtctime.get())
                    mqtt_logger("Time Synchronized! Now: ".. string.format("%04d/%02d/%02d %02d:%02d:%02d", time_now["year"], time_now["mon"], time_now["day"], time_now["hour"], time_now["min"], time_now["sec"]))
                    mqtt_logger("Connected to Broker! ")
                    mqtt_subscribe()
                    tmr.create():alarm(60 * 1000, tmr.ALARM_AUTO, log_idle)
                end)
        end,
        function (client, reason)
            print("Error connecting on MQTT Broker! Reason: " .. reason)
            print("Retrying in 2 seconds")
            tmr.create():alarm(2 * 1000, tmr.ALARM_SINGLE, module.start)
        end)
end

return module