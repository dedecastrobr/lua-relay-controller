local module = {}

module.ID = node.chipid()

module.LOCAL_WIFI = {}
module.LOCAL_WIFI.ssid = "NodeMCU"
module.LOCAL_WIFI.pwd = "12345678"

module.CONFIG_IP = {}  -- set IP,netmask, gateway
module.CONFIG_IP.ip = "192.168.0.99"
module.CONFIG_IP.netmask = "255.255.255.0"
module.CONFIG_IP.gateway = "192.168.0.99"

-- MQTT
module.HOST = "192.168.0.28"
module.PORT = 1883
module.USER = "nodemcu"
module.PASSWD = "r3l3w1f1"


module.RELAYS = {
    ["RELAY_1"] = 0,
    ["RELAY_2"] = 1,
    ["RELAY_3"] = 2,
    ["RELAY_4"] = 3
}


module.ENDPOINT = "relay-controller/"

return module
