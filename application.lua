emeqtt = require ("emeqtt")

local module = {}
mqtt_logger = emeqtt.mqtt_logger
m = emeqtt.m

value = gpio.HIGH
local duration = 100000

local function pulse_relay(relay, topic)
    mqtt_logger("Pulse relay: " .. relay)
    for i = 1, 2 do
        if value == gpio.LOW then
            value = gpio.HIGH
            status = "OFF"
        else
            value = gpio.LOW
            status = "ON"
        end
        gpio.write(relay, value)
        m:publish(topic .. "/state", status, 0, 0)
        mqtt_logger("Publishing to: " .. topic .. "/state")
        mqtt_logger(status)

        tmr.delay(duration)
    end
end

local function turn(relay, state, topic)
    mqtt_logger("TURN: " .. relay .. " => " .. state)
    if state == "ON" then
        st = gpio.LOW
        status = "ON"
    else
        st = gpio.HIGH
        status = "OFF"
    end
    gpio.write(relay, st)
    m:publish(topic .. "/state", status, 0, 0)
end


local function parse_topic(topic, data)
    sw = string.sub(topic,-3)
    if sw == "CV1" then
        if data == "OPEN" then
            pulse_relay(0,topic)
        elseif data == "STOP" then
            pulse_relay(1,topic)
        elseif data == "CLOSE" then
            pulse_relay(2,topic)
        end
    elseif sw == "LT1" then
        turn(3,data,topic)
    elseif sw == "RST" then
        node.restart()
    end
end

local function mqtt_start()
    emeqtt.start()
    m:on("message",
        function(conn, topic, data)
            if data ~= nil then
                parse_topic(topic, data)
            end
        end)
    m:on("offline",
        function ()
            print("MQTT Broker down! Trying again...")
            emeqtt.start()
         end)
end

function module.start()
    config = require("config")
    for i,p in ipairs(config.RELAYS) do
        gpio.mode(p, gpio.OUTPUT)
        gpio.write(p, value)
    end
    mqtt_start()
end

return module
